# CATi10 Admin Frontend

## How to build

```bash
cd stack/admin-ui/admin-ui

npm run build
```

This will produce a newly transpiled front end app under the appropriate 
directory (by default under /var/www)


