module.exports = {
  "fields": {
    "input": [
      "start",
      "end",
      "agent",
      "jobno",
      "activity",
      "hours",
      "completes",
      "call_rate",
      "records"
    ],

    "output": [
      "start",
      "end",
      "agent",
      "jobno",
      "activity",
      "hours",
      "completes",
      "call_rate",
      "records"
    ]
  },

  "transformFunctions": {
    start(row) {
      return row.start.replace(/"/g, '');
    },
    end(row) {
      return row.end.replace(/"/g, '');
    },
    agent(row){ return row.agent.replace(/"/g, '') },
    jobno(row){ return row.jobno.replace(/"/g, '') },
    activity(row) { return row.activity.replace(/"/g, '') },
    hours(row){ return parseFloat(row.hours) },
    completes(row) { return parseFloat(row.completes) },
    call_rate(row) { return parseFloat(row.call_rate) },
    records(row) { return parseFloat(row.records) }
  },

  "types": {
    "input": {
      "name": "string",
      "brand"(inputVal) {
        return (typeof inputVal === 'string' && inputVal.length > 0);
      }
    }
  }
}
