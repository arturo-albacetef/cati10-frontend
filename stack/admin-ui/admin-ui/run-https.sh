#!/bin/bash

root_path=$( dirname $(realpath "$0"))
pem_dir="$root_path/pem"
dist_dir="$root_path/dist"
port=8080

# why not localhost? because then you can't test from a mobile phone on the same network!
export ip_addr="$1"
[[ "$ip_addr" == "" ]] && ip_addr="10.50.0.147";

if [[ ! -d "$pem_dir" ]]; then
    mkdir "$pem_dir"
    cd "$pem_dir"
    openssl req -newkey rsa:2048 -new -nodes -x509 -days 3650 -keyout key.pem -out cert.pem
    cd -
fi

http-server "$dist_dir" -a "$ip_addr" -p "$port" -S -C "$pem_dir/cert.pem" -K "$pem_dir/key.pem" --static static
