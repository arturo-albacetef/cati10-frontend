// http://eslint.org/docs/user-guide/configuring

module.exports = {
    root: true,
    parser: 'babel-eslint',
    parserOptions: { sourceType: 'module' },
    env: { browser: true },



    extends: 'airbnb-base',
    // required to lint *.vue files
    plugins: [ 'html' ],
    // check if imports actually resolve
    'settings': {
        'import/resolver': {
            'webpack': {
                'config': 'build/webpack.base.conf.js'
            }
        }
    },
    // add your custom rules here
    'rules': {
        // don't require .vue extension when importing
        'import/extensions': ['error', 'always', {
            'js': 'never',
            'vue': 'never'
        }],
        // allow optionalDependencies
        'import/no-extraneous-dependencies': ['error', {
            'optionalDependencies': ['test/unit/index.js']
        }],
        // allow debugger during development
        'no-debugger': process.env.NODE_ENV === 'production' ? 2 : 0,

        // custom added
        "indent": ["error", 2],
        "import/first": false,
        'comma-dangle': 0,
        "space-in-parens": 0,
        "dot-notation": 0,
        "arrow-parens": 0,
        "no-underscore-dangle": 0,
        "array-bracket-spacing": 0,
        "prefer-const": 0,
        "no-nested-ternary": 0,
        "spaced-comment": 0,
        'no-prototype-builtins': 0,
        "key-spacing": 0,
        "no-alert": 0
    }
}
