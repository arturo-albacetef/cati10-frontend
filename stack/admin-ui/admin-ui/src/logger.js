/* eslint-disable */
const getTime = () => ( new Date() ).toUTCString();

export default {
  init() {
    this._log = this._log.bind(this);
    this.info = this.info.bind(this);
    this.error = this.error.bind(this);
    this.debug = this.debug.bind(this);
  },
  _log(...args) { console.log(...args); },
  info(...args) { this._log(`[ INFO | ${getTime()}] `, ...args); },
  error(...args) { this._log(`[ ERROR | ${getTime()}] `, ...args); },
  debug(...args) { this._log(`[ DEBUG | ${getTime()}] `, ...args); }
};
