// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue';
import App from './App';
import router from './router';
import Vuetify from 'vuetify';
import 'vuetify/dist/vuetify.min.css';
import { loadComponents } from '@/components/load-components';

window.Vue = Vue;
loadComponents(Vue);

// eslint-disable-next-line
Notification.requestPermission(function(status) {
  // eslint-disable-next-line
  console.log('Notification permission status:', status);
});

Vue.use(require('vue-chartist'), {
  messageNoData: 'You have insufficient data',
  classNoData: 'empty'
});

Vue.use(
  Vuetify,
  {
    theme: {
      primary: '#ee44aa',
      secondary: '#424242',
      accent: '#82B1FF',
      error: '#FF5252',
      info: '#2196F3',
      success: '#4CAF50',
      warning: '#FFC107'
    }
  }
);

Vue.config.productionTip = false;

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>'
});
