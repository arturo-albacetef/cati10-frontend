import PageTitle from '@/components/ui-gallery/PageTitle';
import ExportToCSV from '@/components/ui-gallery/ExportToCSV';
import SnackBar from '@/components/ui-gallery/SnackBar';
import SearchBar from '@/components/ui-gallery/SearchBar';
import VueQueryBuilder from '@/components/ui-gallery/VueQueryBuilder';
import ToolTip from '@/components/ui-gallery/ToolTip';
/*

@TODO
  this would be better not mutable, but returns a list of { name, comp }
  which are then looped over and Vue.component(name, comp) is assigned

  makes sense for tag to be defined in a single file for all components (reusable)
*/

// eslint-disable-next-line
export function loadComponents(__VUE) {
  __VUE.component('page-title', PageTitle);
  __VUE.component('export-to-csv', ExportToCSV);
  __VUE.component('vue-query-builder', VueQueryBuilder);
  __VUE.component('snackbar', SnackBar);
  __VUE.component('search-bar', SearchBar);
  __VUE.component('tool-tip', ToolTip);
}
