import Vue from 'vue';
import Router from 'vue-router';
import Overview from '@/components/overview/Overview';
import Login from '@/components/Login';
import Agents from '@/components/agents/Agents';
import Jobs from '@/components/jobs/Jobs';
import Timesheets from '@/components/timesheets/Timesheets';
import PDS from '@/components/pds/PDS';
import JobRules from '@/components/job-rules/JobRules';


Vue.use(Router);

export default new Router({
  routes: [
    {
      path: '/',
      redirect: '/overview'
    },
    {
      path: '/overview',
      name: 'Overview',
      component: Overview
    },
    {
      path: '/agents',
      name: 'Agents',
      component: Agents
    },
    {
      path: '/login',
      name: 'Login',
      component: Login
    },
    {
      path: '/jobs',
      name: 'Jobs',
      component: Jobs
    },
    {
      path: '/timesheets',
      name: 'Timesheets',
      component: Timesheets
    },
    {
      path: '/pds',
      name: 'PDS',
      component: PDS
    },
    {
      path: '/job-rules',
      name: 'Job Rules',
      component: JobRules
    }
  ]
});
