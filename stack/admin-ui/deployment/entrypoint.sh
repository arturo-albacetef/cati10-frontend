#!/bin/sh

export app_dir=/app
export NPM_CONFIG_PREFIX=/home/node/npm-global

cd $app_dir

# install packages
npm i
npm rebuild node-sass

# build
npm run build
